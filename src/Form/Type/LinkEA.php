<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 06.11.2018
 * Time: 18:08
 */

namespace App\Form\Type;

use App\Form\DataTransformer\LinkEditorEasyAdmin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class LinkEA extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addViewTransformer(new LinkEditorEasyAdmin());
    }

    public function getParent()
    {
        return TextareaType::class;
    }

}