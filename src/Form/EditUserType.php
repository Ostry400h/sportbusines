<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 30.08.2018
 * Time: 15:10
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\DependencyInjection\Compiler\RepeatedPass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',null,['disabled'=> true])
            ->add('name')
            ->add('surname')
            ->add('phone')
            ->add('position')
            ->add('companyName')
            ->add('companyAddress')
            ->add('companyPostal')
            ->add('companyCity')
            ->add('companyTax')
            ->add('sendName')
            ->add('sendAddress')
            ->add('sendPostal')
            ->add('sendCity')
            ->add('sendInfo');
//            ->add('plainPassword', RepeatedType::class , [
//                'type' => PasswordType::class
//            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class]//,
                //            'validation_groups' =>['Default', 'Registration']
//        ]
        );
    }
}
