<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 06.11.2018
 * Time: 18:10
 */

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;


class LinkEditorEasyAdmin implements DataTransformerInterface
{
    public function transform($value)
    {
        return str_replace(" ","_+_",$value);
    }

    public function reverseTransform($value)
    {
        return $value;
    }
}