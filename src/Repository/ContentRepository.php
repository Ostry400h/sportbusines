<?php

namespace App\Repository;

use App\Entity\Content;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Content|null find($id, $lockMode = null, $lockVersion = null)
 * @method Content|null findOneBy(array $criteria, array $orderBy = null)
 * @method Content[]    findAll()
 * @method Content[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Content::class);
    }

    /**
     * @param $value
     * @return Content[]
     */

    public function getContentsInProduct($value)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.product','p')
            ->andWhere('p.id = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
    /**
     * @param $value
     * @return Content[]
     */

    public function getContentsInCategory($value)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.category','cat')
            ->andWhere('cat.id = :val')
            ->andWhere('cat.isHidden = 0')
            ->setParameter('val', $value)
            ->orderBy('cat.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

  /**
     * @param $value
     * @return Content[]
     */

    public function getContentsSegments(array $orders,$category)
    {
        return $this->createQueryBuilder('c')

            ->innerJoin('c.category','cat')
            ->leftJoin('c.person','person')
            ->leftJoin('person.company', 'cp')
            ->andWhere('cat.name = :category')
            ->andWhere('c.id IN (:ordersC)')
            ->setParameters(['ordersC'=>$orders,'category' => $category])
//            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


//    /**
//     * @return Content[] Returns an array of Content objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Content
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
