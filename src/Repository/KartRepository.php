<?php

namespace App\Repository;

use App\Entity\Kart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Kart|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kart|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kart[]    findAll()
 * @method Kart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KartRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Kart::class);
    }

    /**
     * @return Kart[] Returns an array of Kart objects
     */

    public function getUserCart($value,$name)
    {
//        return $this->createQueryBuilder('k')
//            ->andWhere('k.'.$name.' = :val')
//            ->setParameter('val', $value)
//            ->orderBy('k.createdAtValue')
////            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult();

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT k,
                (k.productRetailPrice * k.productQuantity)as totalProd
                FROM App\Entity\Kart k
                WHERE k.' . $name . ' = :value
                and k.orderId IS NULL
                ORDER BY k.createdAtValue')
            ->setParameter('value', $value);


        // returns an array of Product objects
        return $query->execute();
    }
    public function getUserTotal($value,$name)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT 
                SUM(k.productRetailPrice * k.productQuantity)as total,
                SUM(k.weight * k.productQuantity) AS sumweight 
                FROM App\Entity\Kart k
                WHERE k.' . $name . ' = :value
                and k.orderId IS NULL')
            ->setParameter('value', $value);

        // returns an array of Product objects
        return $query->execute();
    }
}
