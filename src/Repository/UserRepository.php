<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method user|null find($id, $lockMode = null, $lockVersion = null)
 * @method user|null findOneBy(array $criteria, array $orderBy = null)
 * @method user[]    findAll()
 * @method user[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, user::class);
    }

//    /**
//     * @return user[] Returns an array of user objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    /**
     * @param $value
     * @return User[]
     */

    public function findIaiUserExist($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :val')
            ->andWhere('u.iaiId IS NOT NULL')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $value
     * @return User[]
     */

    public function getUserProduct($idUser,$idProduct)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.products', 'prod')
            ->andWhere('prod.id = :product')
            ->andWhere('u.id = :user')
//            ->andWhere('prod.isHidden = 0')
            ->setParameter('product', $idProduct)
            ->setParameter('user', $idUser)
            //            ->orderBy('prod.id', 'ASC')
            ->getQuery()
//            ->setMaxResults(1)
            ->getResult();
    }

}
