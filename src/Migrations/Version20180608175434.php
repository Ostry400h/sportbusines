<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180608175434 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, icon VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL, availability VARCHAR(255) NOT NULL, is_published TINYINT(1) NOT NULL, published_date DATETIME NOT NULL, content_value LONGTEXT DEFAULT NULL, INDEX IDX_FEC530A9C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_category (content_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_54FBF32E84A0A3ED (content_id), INDEX IDX_54FBF32E12469DE2 (category_id), PRIMARY KEY(content_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_person (content_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_5CEAA1DA84A0A3ED (content_id), INDEX IDX_5CEAA1DA217BBB47 (person_id), PRIMARY KEY(content_id, person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_product (content_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_C42DD1E84A0A3ED (content_id), INDEX IDX_C42DD1E4584665A (product_id), PRIMARY KEY(content_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_content (content_source INT NOT NULL, content_target INT NOT NULL, INDEX IDX_21CDE91A37BC0FDF (content_source), INDEX IDX_21CDE91A2E595F50 (content_target), PRIMARY KEY(content_source, content_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_description (id INT AUTO_INCREMENT NOT NULL, content_id INT NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_3ECEEAA084A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parameter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, icon VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, company VARCHAR(255) DEFAULT NULL, position VARCHAR(255) DEFAULT NULL, position_redaction VARCHAR(255) DEFAULT NULL, child INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_parameter (person_id INT NOT NULL, parameter_id INT NOT NULL, INDEX IDX_92BE72BA217BBB47 (person_id), INDEX IDX_92BE72BA7C56DBD6 (parameter_id), PRIMARY KEY(person_id, parameter_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, nr INT NOT NULL, year INT DEFAULT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) DEFAULT NULL, cover VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, nr_issn_issb VARCHAR(255) DEFAULT NULL, publisher VARCHAR(255) DEFAULT NULL, is_published TINYINT(1) NOT NULL, published_date DATETIME NOT NULL, INDEX IDX_D34A04ADC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_person (product_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_56A090D24584665A (product_id), INDEX IDX_56A090D2217BBB47 (person_id), PRIMARY KEY(product_id, person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_parameter (product_id INT NOT NULL, parameter_id INT NOT NULL, INDEX IDX_4437279D4584665A (product_id), INDEX IDX_4437279D7C56DBD6 (parameter_id), PRIMARY KEY(product_id, parameter_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A9C54C8C93 FOREIGN KEY (type_id) REFERENCES content_type (id)');
        $this->addSql('ALTER TABLE content_category ADD CONSTRAINT FK_54FBF32E84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_category ADD CONSTRAINT FK_54FBF32E12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_person ADD CONSTRAINT FK_5CEAA1DA84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_person ADD CONSTRAINT FK_5CEAA1DA217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_product ADD CONSTRAINT FK_C42DD1E84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_product ADD CONSTRAINT FK_C42DD1E4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_content ADD CONSTRAINT FK_21CDE91A37BC0FDF FOREIGN KEY (content_source) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_content ADD CONSTRAINT FK_21CDE91A2E595F50 FOREIGN KEY (content_target) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_description ADD CONSTRAINT FK_3ECEEAA084A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE person_parameter ADD CONSTRAINT FK_92BE72BA217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_parameter ADD CONSTRAINT FK_92BE72BA7C56DBD6 FOREIGN KEY (parameter_id) REFERENCES parameter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADC54C8C93 FOREIGN KEY (type_id) REFERENCES product_type (id)');
        $this->addSql('ALTER TABLE product_person ADD CONSTRAINT FK_56A090D24584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_person ADD CONSTRAINT FK_56A090D2217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_parameter ADD CONSTRAINT FK_4437279D4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_parameter ADD CONSTRAINT FK_4437279D7C56DBD6 FOREIGN KEY (parameter_id) REFERENCES parameter (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE content_category DROP FOREIGN KEY FK_54FBF32E12469DE2');
        $this->addSql('ALTER TABLE content_category DROP FOREIGN KEY FK_54FBF32E84A0A3ED');
        $this->addSql('ALTER TABLE content_person DROP FOREIGN KEY FK_5CEAA1DA84A0A3ED');
        $this->addSql('ALTER TABLE content_product DROP FOREIGN KEY FK_C42DD1E84A0A3ED');
        $this->addSql('ALTER TABLE content_content DROP FOREIGN KEY FK_21CDE91A37BC0FDF');
        $this->addSql('ALTER TABLE content_content DROP FOREIGN KEY FK_21CDE91A2E595F50');
        $this->addSql('ALTER TABLE content_description DROP FOREIGN KEY FK_3ECEEAA084A0A3ED');
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A9C54C8C93');
        $this->addSql('ALTER TABLE person_parameter DROP FOREIGN KEY FK_92BE72BA7C56DBD6');
        $this->addSql('ALTER TABLE product_parameter DROP FOREIGN KEY FK_4437279D7C56DBD6');
        $this->addSql('ALTER TABLE content_person DROP FOREIGN KEY FK_5CEAA1DA217BBB47');
        $this->addSql('ALTER TABLE person_parameter DROP FOREIGN KEY FK_92BE72BA217BBB47');
        $this->addSql('ALTER TABLE product_person DROP FOREIGN KEY FK_56A090D2217BBB47');
        $this->addSql('ALTER TABLE content_product DROP FOREIGN KEY FK_C42DD1E4584665A');
        $this->addSql('ALTER TABLE product_person DROP FOREIGN KEY FK_56A090D24584665A');
        $this->addSql('ALTER TABLE product_parameter DROP FOREIGN KEY FK_4437279D4584665A');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADC54C8C93');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE content');
        $this->addSql('DROP TABLE content_category');
        $this->addSql('DROP TABLE content_person');
        $this->addSql('DROP TABLE content_product');
        $this->addSql('DROP TABLE content_content');
        $this->addSql('DROP TABLE content_description');
        $this->addSql('DROP TABLE content_type');
        $this->addSql('DROP TABLE parameter');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE person_parameter');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_person');
        $this->addSql('DROP TABLE product_parameter');
        $this->addSql('DROP TABLE product_type');
    }
}
