<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180620211701 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE content_person');
        $this->addSql('ALTER TABLE person ADD mail VARCHAR(255) DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD publisher_id VARCHAR(255) DEFAULT NULL, ADD company_send_name VARCHAR(255) DEFAULT NULL, ADD send_person VARCHAR(255) DEFAULT NULL, ADD send_address VARCHAR(255) DEFAULT NULL, ADD send_postcode VARCHAR(255) DEFAULT NULL, ADD send_city VARCHAR(255) DEFAULT NULL, ADD send_mail VARCHAR(255) DEFAULT NULL, ADD send_phone VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE content_person (content_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_5CEAA1DA84A0A3ED (content_id), INDEX IDX_5CEAA1DA217BBB47 (person_id), PRIMARY KEY(content_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content_person ADD CONSTRAINT FK_5CEAA1DA217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_person ADD CONSTRAINT FK_5CEAA1DA84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person DROP mail, DROP phone, DROP publisher_id, DROP company_send_name, DROP send_person, DROP send_address, DROP send_postcode, DROP send_city, DROP send_mail, DROP send_phone');
    }
}
