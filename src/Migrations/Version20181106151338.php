<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181106151338 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE person_content (person_id INT NOT NULL, content_id INT NOT NULL, INDEX IDX_DC5B8320217BBB47 (person_id), INDEX IDX_DC5B832084A0A3ED (content_id), PRIMARY KEY(person_id, content_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE person_content ADD CONSTRAINT FK_DC5B8320217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_content ADD CONSTRAINT FK_DC5B832084A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE company_content');
        $this->addSql('DROP TABLE company_product');
        $this->addSql('ALTER TABLE company ADD company_date DATE NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_content (company_id INT NOT NULL, content_id INT NOT NULL, INDEX IDX_DE972A7E979B1AD6 (company_id), INDEX IDX_DE972A7E84A0A3ED (content_id), PRIMARY KEY(company_id, content_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_product (company_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_F3181E7A979B1AD6 (company_id), INDEX IDX_F3181E7A4584665A (product_id), PRIMARY KEY(company_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_content ADD CONSTRAINT FK_DE972A7E84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_content ADD CONSTRAINT FK_DE972A7E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_product ADD CONSTRAINT FK_F3181E7A4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_product ADD CONSTRAINT FK_F3181E7A979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE person_content');
        $this->addSql('ALTER TABLE company DROP company_date');
        $this->addSql('ALTER TABLE user CHANGE roles roles VARCHAR(300) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
