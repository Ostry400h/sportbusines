<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180830131221 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) NOT NULL, ADD surname VARCHAR(255) NOT NULL, ADD phone VARCHAR(255) DEFAULT NULL, ADD position VARCHAR(255) DEFAULT NULL, ADD company_name VARCHAR(255) DEFAULT NULL, ADD company_address VARCHAR(255) DEFAULT NULL, ADD company_postal VARCHAR(255) DEFAULT NULL, ADD company_city VARCHAR(255) DEFAULT NULL, ADD company_tax VARCHAR(255) DEFAULT NULL, ADD send_name VARCHAR(255) DEFAULT NULL, ADD send_address VARCHAR(255) DEFAULT NULL, ADD send_postal VARCHAR(255) DEFAULT NULL, ADD send_city VARCHAR(255) DEFAULT NULL, ADD send_info LONGTEXT DEFAULT NULL, ADD roles LONGTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP name, DROP surname, DROP phone, DROP position, DROP company_name, DROP company_address, DROP company_postal, DROP company_city, DROP company_tax, DROP send_name, DROP send_address, DROP send_postal, DROP send_city, DROP send_info, DROP roles');
    }
}
