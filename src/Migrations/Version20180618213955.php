<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180618213955 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, position VARCHAR(255) DEFAULT NULL, position_redaction VARCHAR(255) DEFAULT NULL, INDEX IDX_4FBF094F217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_product (company_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_F3181E7A979B1AD6 (company_id), INDEX IDX_F3181E7A4584665A (product_id), PRIMARY KEY(company_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_content (company_id INT NOT NULL, content_id INT NOT NULL, INDEX IDX_DE972A7E979B1AD6 (company_id), INDEX IDX_DE972A7E84A0A3ED (content_id), PRIMARY KEY(company_id, content_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE company_product ADD CONSTRAINT FK_F3181E7A979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_product ADD CONSTRAINT FK_F3181E7A4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_content ADD CONSTRAINT FK_DE972A7E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_content ADD CONSTRAINT FK_DE972A7E84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE content_description');
        $this->addSql('DROP TABLE person_parameter');
        $this->addSql('ALTER TABLE person DROP company, DROP position, DROP position_redaction, DROP child');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_product DROP FOREIGN KEY FK_F3181E7A979B1AD6');
        $this->addSql('ALTER TABLE company_content DROP FOREIGN KEY FK_DE972A7E979B1AD6');
        $this->addSql('CREATE TABLE content_description (id INT AUTO_INCREMENT NOT NULL, content_id INT NOT NULL, description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_3ECEEAA084A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_parameter (person_id INT NOT NULL, parameter_id INT NOT NULL, INDEX IDX_92BE72BA217BBB47 (person_id), INDEX IDX_92BE72BA7C56DBD6 (parameter_id), PRIMARY KEY(person_id, parameter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content_description ADD CONSTRAINT FK_3ECEEAA084A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE person_parameter ADD CONSTRAINT FK_92BE72BA217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_parameter ADD CONSTRAINT FK_92BE72BA7C56DBD6 FOREIGN KEY (parameter_id) REFERENCES parameter (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE company_product');
        $this->addSql('DROP TABLE company_content');
        $this->addSql('ALTER TABLE person ADD company VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD position VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD position_redaction VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD child INT DEFAULT NULL');
    }
}
