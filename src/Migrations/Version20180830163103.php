<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180830163103 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE kart (id INT AUTO_INCREMENT NOT NULL, user_id_id INT DEFAULT NULL, summary_price NUMERIC(10, 2) NOT NULL, shiping_price NUMERIC(10, 2) NOT NULL, is_done TINYINT(1) NOT NULL, INDEX IDX_CE17A0589D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kart_product (kart_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_2E9CBA54293A83D8 (kart_id), INDEX IDX_2E9CBA544584665A (product_id), PRIMARY KEY(kart_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE kart ADD CONSTRAINT FK_CE17A0589D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE kart_product ADD CONSTRAINT FK_2E9CBA54293A83D8 FOREIGN KEY (kart_id) REFERENCES kart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kart_product ADD CONSTRAINT FK_2E9CBA544584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kart_product DROP FOREIGN KEY FK_2E9CBA54293A83D8');
        $this->addSql('DROP TABLE kart');
        $this->addSql('DROP TABLE kart_product');
    }
}
