<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180830231929 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE kart_product');
        $this->addSql('ALTER TABLE kart ADD product_id INT NOT NULL, ADD product_quantity INT NOT NULL, ADD product_retail_price NUMERIC(10, 2) NOT NULL, ADD product_vat NUMERIC(10, 2) NOT NULL, ADD product_bundle_items VARCHAR(255) DEFAULT NULL, DROP summary_price, DROP shiping_price');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE kart_product (kart_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_2E9CBA54293A83D8 (kart_id), INDEX IDX_2E9CBA544584665A (product_id), PRIMARY KEY(kart_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE kart_product ADD CONSTRAINT FK_2E9CBA54293A83D8 FOREIGN KEY (kart_id) REFERENCES kart (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kart_product ADD CONSTRAINT FK_2E9CBA544584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kart ADD summary_price NUMERIC(10, 2) NOT NULL, ADD shiping_price NUMERIC(10, 2) NOT NULL, DROP product_id, DROP product_quantity, DROP product_retail_price, DROP product_vat, DROP product_bundle_items');
    }
}
