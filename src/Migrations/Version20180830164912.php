<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180830164912 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kart DROP FOREIGN KEY FK_CE17A0589D86650F');
        $this->addSql('DROP INDEX IDX_CE17A0589D86650F ON kart');
        $this->addSql('ALTER TABLE kart ADD payment_status VARCHAR(255) DEFAULT NULL, ADD payment_id VARCHAR(255) DEFAULT NULL, ADD weight NUMERIC(10, 2) NOT NULL, CHANGE user_id_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE kart ADD CONSTRAINT FK_CE17A058A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_CE17A058A76ED395 ON kart (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kart DROP FOREIGN KEY FK_CE17A058A76ED395');
        $this->addSql('DROP INDEX IDX_CE17A058A76ED395 ON kart');
        $this->addSql('ALTER TABLE kart DROP payment_status, DROP payment_id, DROP weight, CHANGE user_id user_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE kart ADD CONSTRAINT FK_CE17A0589D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_CE17A0589D86650F ON kart (user_id_id)');
    }
}
