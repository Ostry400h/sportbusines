<?php
/**
 * Created by PhpStorm.
 * user: Rafal
 * Date: 23.05.2018
 * Time: 13:56
 */

namespace App\Controller;



use App\Entity\Category;
use App\Entity\Content;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\ContentRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class JanekController extends AbstractController
{

 
    /**
     * @Route("/sportbiz2018", name="sportbiz2018")
     */

    public function sportbiz2018(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('sportbiz2018.html.twig',
            [
                'categories' => $category,
            ]);
    }



    /**
     * @Route("/biblioteczka", name="biblioteczka")
     */

    public function biblioteczka(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('biblioteczka.html.twig',
            [
                'categories' => $category,
            ]);
    }


    /**
     * @Route("/article", name="article")
     */

    public function article(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('article.html.twig',
            [
                'categories' => $category,
            ]);
    }




    /**
     * @Route("/index3", name="index3")
     */

    public function index3(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('index3.html.twig',
            [
                'categories' => $category,
            ]);
    }




    /**
     * @Route("/index2", name="index2")
     */

    public function index2(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('index2.html.twig',
            [
                'categories' => $category,
            ]);
    }


    /**
     * @Route("/csr", name="csr")
     */

    public function csr(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('csr.html.twig',
            [
                'categories' => $category,
            ]);
    }


}