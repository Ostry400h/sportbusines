<?php
/**
 * Created by PhpStorm.
 * user: Rafal
 * Date: 23.05.2018
 * Time: 13:56
 */

namespace App\Controller;



use App\Entity\Category;
use App\Entity\Content;
use App\Entity\Person;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\ContentRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class ArticleController extends AbstractController
{

    /**
     * @Route("/", name="app_homepage")
     */

    public function homepage(ContentRepository $contentRepo, EntityManagerInterface $em)
    {


        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        $repository = $em->getRepository(Product::class);
        $product = $repository->findAll();

        $ids = [1,2,3,4];
        $fromCategory = 'nowa';

        $opinion = $contentRepo->getContentsSegments($ids,$fromCategory);

        $ids = [1,2,3,4];
        $fromCategory = 'wydarzenia';

        $wydarzenia = $contentRepo->getContentsSegments($ids,$fromCategory);

        $ids = [1,2,3,4];
        $fromCategory = 'kariera';

        $kariera = $contentRepo->getContentsSegments($ids,$fromCategory);
//        $opinion = $contentRepo->getContentsInProduct($id);

//        $repository = $em->getRepository(Person::class):
//        $person = $repository->
        return $this->render('index2.html.twig',
        [
            'categories' => $category,
            'products' => $product,
            'opinions' =>$opinion,
            'kariera' => $kariera,
            'wydarzenia' => $wydarzenia
        ]);
    }
    /**
     * @Route("/category/{id}", name="listContentCategory")
     */

    public function listContentInCategory($id, EntityManagerInterface $em, ContentRepository $repository)
    {
        $repositoryCat = $em->getRepository(Category::class);
        $category = $repositoryCat->getCategories();

//        $contentRepository = $em->getRepository(Category::class);
        $content = $repository->getContentsInCategory($id);


        return $this->render('content/list.html.twig',
            [
                'categories' => $category,
                'contents' => $content
            ]);
    }


    /**
     * @Route("/product/{id}", name="listContentProduct")
     */

    public function listContentInProduct($id, ContentRepository $repository, EntityManagerInterface $em)
    {
//        $repositoryEm = $em->getRepository(Content::class);
            $content = $repository->getContentsInProduct($id);

        $repositoryEm = $em->getRepository(Category::class);
        $category = $repositoryEm->getCategories();

        return $this->render('content/list.html.twig',
            [
                'contents' => $content,
                'categories' => $category
//            'comments' => $comments,
            ]);
    }

    /**
     * @Route("/content/{linkPage}", name="showContent")
     */

    public function showContent($linkPage, EntityManagerInterface $em,UserInterface $user = null, UserRepository $repositoryUser)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

                /** @var Content $repository */
        $repositoryContent = $em->getRepository(Content::class);
        $content = $repositoryContent->findOneBy(['linkPage' => $linkPage]);

//        $magazyn = $content->getProduct();

        foreach($content->getProduct() as $product){
            $magazyn[] = $product->getPublishedDate();
        }
        $magazyn = min($magazyn);
        $autorFirma =[];
        foreach($content->getPerson() as $person){

            foreach ($person->getCompany() as $personCompany) {
                if ($personCompany->getCompanyDate() < $magazyn) {
                    $wys[] = $personCompany;
                }
            }
            $autorFirma[] = ['person' => $person, 'company' => max($wys)];
        }

        if($content->getAvailability() == 'zalogowani')
        {
//            Both options working
//              if need more option chose this with ROLE

//           $data = ($this->isGranted('ROLE_USER') == false)? 'cut': 'all';
           $data = ($user == null)? 'cut': 'all';

        }
        elseif($content->getAvailability() == 'wykupiony') {
            if ($user == null) {
                $data = 'cut';
            } else {

                $contentProduct = $content->getProduct();
                $data = ($repositoryUser->getUserProduct($user->getId(),$contentProduct))? 'all' : 'cut';
            }
        }
        else {
            $data = 'all';
        }

        return $this->render('content/show.html.twig',
            [
                'data' => $data,
                'content' => $content,
                'categories' => $category,
                'magazyn' => $magazyn,
                'autorFirma' => $autorFirma
//                'datamaga' => $wys
//            'comments' => $comments,
            ]);
    }

    /**
     * @Route("/content", name="listAllContent")
     */

    public function listContentAll(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Content::class);
        $content = $repository->findAll();

        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('content/list.html.twig',
            [
                'contents' => $content,
                'categories' => $category
//            'comments' => $comments,
            ]);
    }

    public function showProducts(EntityManagerInterface $em)
    {
        $repositoryCat = $em->getRepository(Category::class);
        $category = $repositoryCat->getCategories();


        // TODO zrobic listowanie wszystkich produków z id shop iai + front

        return $this->render('content/shop.html.twig',
            [
                'data' => $data,
                'categories' => $category
//            'comments' => $comments,
            ]);
    }


    /**
     * @Route("/subcontent", name="subcon")
     */

    public function showSub(EntityManagerInterface $em, $publisherId)
    {
        $repository = $em->getRepository(Content::class);
        $content = $repository->findOneBy(['publisherId' => $publisherId]);

        return $this->render('content/show2.html.twig',
        [
            'content' => $content,
            'max'=> 4
        ]);
    }
    /**
     * @Route("/sportbiz2018", name="sportbiz2018")
     */

    public function sportbiz2018(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('sportbiz2018.html.twig',
            [
                'categories' => $category,
            ]);
    }



    /**
     * @Route("/biblioteczka", name="biblioteczka")
     */

    public function biblioteczka(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('biblioteczka.html.twig',
            [
                'categories' => $category,
            ]);
    }


    /**
     * @Route("/article", name="article")
     */

    public function article(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('article.html.twig',
            [
                'categories' => $category,
            ]);
    }




    /**
     * @Route("/index3", name="index3")
     */

    public function index3(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('index3.html.twig',
        [
            'categories' => $category,
        ]);
    }


	/**
     * @Route("/koszyk", name="koszyk")
     */

    public function koszyk(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('koszyk.html.twig',
        [
            'categories' => $category,
        ]);
    }




    /**
     * @Route("/index2", name="index2")
     */

    public function index2(EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('index2.html.twig',
            [
                'categories' => $category,
            ]);
    }


    /**
     * @Route("/csr/{values}", name="csr")
     */

    public function csr(EntityManagerInterface $em, Request $request)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        return $this->render('csr.html.twig',
            [
                'categories' => $category,
                'rest' => $request
            ]);
    }

}