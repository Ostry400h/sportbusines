<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 17.07.2018
 * Time: 21:07
 */

namespace App\Controller;


use Allset\Przelewy24Bundle\Factory\ProcessFactory;
use Allset\Przelewy24Bundle\Model\Payment;
use App\Entity\Category;
use App\Entity\Kart;
use App\Entity\Payment as paymentDB;
use App\Entity\Product;
use App\Entity\User;
use App\Form\AddUserType;
use App\Form\EditUserType;
use App\Services\cartHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Psr\Container\ContainerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Services\iaiServiceHelper as iaiHelper;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Flex\Response;

class IaiController extends AbstractController
{

    /**
     * @Route("/productIai", name="productIai")
     */

    public function getShopProducts()
    {

        $response = (new iaiHelper())->shopProducts();

//        dump($response['response']->results[0]->productStocksData);die;
        return $this->render('product.html.twig', [
            'status' => $response['status'],
            'response' => $response['response'],
        ]);
    }

    /**
     * @Route("/cart", name="cart")
     */

    public function getCart(Request $request, EntityManagerInterface $em, UserInterface $user = null, UserPasswordEncoderInterface $passwordEncoder)
    {

        $repositoryCat = $em->getRepository(Category::class);
        $category = $repositoryCat->getCategories();


        if($user != null){
            $userId = $user->getId();
            $userType = "user";
            $repository = $em->getRepository(User::class);
            $userData = $repository->find($userId);
            $form = $this->createForm(EditUserType::class, $userData);
        }else{
            $userId = 1000;
            $userType = "tempUser";
            $form = $this->createForm(AddUserType::class);
        }

        $repositoryKart = $em->getRepository(Kart::class);
        $kartData = $repositoryKart->getUserCart($userId,$userType);

        $kartTotal = $repositoryKart->getUserTotal($userId,$userType);

        $priceTotal = (new cartHelper($em))->countShippingPrice($kartTotal);


        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userData = $form->getData();
            if($userType == 'tempUser') {
                // 3) Encode the password (you could also do this via Doctrine listener)
                $userPassword = (empty($userData->getPlainPassword()))? uniqid() : $userData->getPlainPassword();

                $password = $passwordEncoder->encodePassword($userData, $userPassword);
                $userData->setPassword($password);
            }
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userData);
            $entityManager->flush();

            $repository = $em->getRepository(User::class);

//        dump($form->getData()); die;
           $wynik = $repository->findIaiUserExist($userData->getEmail());
//           dump($wynik);die;
                if(isset($wynik)){
               //json_decode(json_encode($wynik),true);

               $response = (new iaiHelper())->addUser($userData);
//               dump($response);die;
        }
        dump($kartData);
        //TODO przeniesienie zakupów z konta anonim do konta usera
            //TODO rejestrujemy zioma i zapisujemy jego ID w bazie

            $data = ['kart' => $kartData, 'price' => $priceTotal,'form' =>$userData ];
            $response = (new iaiHelper())->makeOrder($data);

           $response = json_decode(json_encode($response),true);

           if($response['response']['errors']['faultCode'] == 0 & $response['response']['results']['ordersResults'][0]['faultCode'] == 0)
            {
//               $now = new DateTime();
//TODO DODAC LOGOWANIE AKCJI Z RESPONSA + blokować ekran wyswietlająć jakiś błąd
               dump($response['response']);
               $orderId = $response['response']['results']['ordersResults'][0]['orderId'];
              $payment = new PaymentDB();
              $payment->setOrderId($orderId);
              $payment->setPrice($priceTotal['finalTotal'] * 100);
              $payment->setUniq(uniqid());
//              $payment->setEmail()
//              $payment->setDate($now);

//               $entityManager = $this->getDoctrine()->getManager();

               $em->persist($payment);
               $em->flush();
//


                foreach ($kartData as $obj) {
                    $obj[0]->setOrderId($orderId);
                    $em->flush();

                    //TU JEST TO CO TRZEBA ZUPDATWA PO Płatności ID wydania do usera

//                    $repository = $em->getRepository(User::class);
//                    $userProd = $repository->findOneBy(['email' => $userData->getEmail()]);
//
//                    $product = $em->getRepository(Product::class);
//                    $repo = $product->findOneBy(['shopId' => $obj[0]->getProductId()]);
//                    $userProd->addProduct($repo);
//
//                    $em->flush();
                }
               //TODO TU MA BYC Return do płatności.
               return $this->redirectToRoute('payment',['orderId'=>$orderId] );
           }
            dump($response);

            //TODO zrobić walidacje responsa



        }
        return $this->render('koszyk.html.twig',
            [
                'form' => $form->createView(),
                'categories' => $category,
                'data' => $kartData,
                'userType' => $userType,
                'priceTotal' => $priceTotal,

            ]);

    }
//
//    public function toArray($obj)
//    {
//        if (is_object($obj)) $obj = (array)$obj;
//        if (is_array($obj)) {
//            $new = array();
//            foreach ($obj as $key => $val) {
//                $new[$key] = $val;
//            }
//        } else {
//            $new = $obj;
//        }
//
//        return $new;
//    }

    /**
     * @Route("/payment/{orderId}", name="payment")
     */
    public function processAction($orderId, ProcessFactory $processFactory, EntityManagerInterface $em)
    {
        $repository = $em->getRepository(PaymentDB::class);
        $order = $repository->findOneBy(['orderId'=>$orderId]);
        // ... - You are creating your order here
        dump($order);
        $payment = new Payment();
        $payment
            ->setCurrency('PLN')
            ->setSessionId(uniqid()) //VERY IMPORTANT some unique id from your order in your db
            ->setAmount('10')
            ->setDescription("magazyn")
            ->setEmail('ostry400h@gmail.com')
            ->setReturnUrl($this->generateUrl('app_homepage', [], 0)); // use following syntax to genreate absolute url

        $processFactory->setPayment($payment);
        $url = $processFactory->createAndGetUrl();


        return $this->redirect($url);
    }
        /**
     * @Route("/addtocart/{id}", name="addtocart")
     */

    public function addToCart($id, EntityManagerInterface $em,  UserInterface $user = null)
    {

        if($user == null)
        {
            $session = $this->get('session');

            if(($session->get('userAnon'))){
                $session->set('userAnon',rand(1000,9999));
            }
            $user = $session->get('userAnon');
        }
        (new cartHelper($em))->addToCartHelper($id,$user);

        return $this->redirectToRoute('cart');

    }

//    /**
//     * @Route("/addUser", name="addUser")
//     */
//    public function addUser($value)
//    {
//
//        $data = [];
//        $data['login'] = "ostry400h@gmail.com";
//        $data['code_extern'] = "1";
//        $data['email'] = "ostry400h@gmail.com";
//        $data['firstname'] = "Rafał";
//        $data['lastname'] = "Ostrowski";
//        $data['street'] = "Na Młyny";
//        $data['zipcode'] = "33-112";
//        $data['city'] = "Tarnowiec";
////$data['country_code'] = "";
////$data['province_code'] = "";
//        $data['password'] = "12345678";
////$data['birth_date'] = "";
//        $data['phone'] = "792544024";
//        $data['company'] = "JKR";
//        $data['shop'] = 3;
//
//        $response = (new iaiHelper())->addUser($data);
//
//        dump($response);
//        die;
////        $view = $this->view($response);
////        return $this->handleView($view);
////        return [$response['response']];
////        return $this->render('product.html.twig', [
////            'status'          => $response['status'],
////            'response'         => $response['response'],
////        ]);
//    }
    
    /**
     *@Route("/order/{id}", name="order")
     */

    public function orderProduct($id,UserInterface $user = null)
    {
//        if($user != null){
//            $userId = $user->getId();
//            $userType = "user";
//        }else{
//            $userId = 1000;
//            $userType = "tempUser";
//        }

        //TODO sprawdzenie czy ziom ma już konto w IAI

        $response = (new iaiHelper())->addUser($data);

        $data = [];
        $data["clientLogin"] = "ostry400h@gmail.com";
        $data["clientNoteToOrder"] = "siema moje pierwsze zakupy przez API";
        $data["deliveryCost"] = 6.0;
        $data["clientDeliveryAddressFirstName"] = "Rafał";
        $data["clientDeliveryAddressLastName"] = "Ostrowski";
        $data["clientDeliveryAddressAdditional"] = "a";
        $data["clientDeliveryAddressStreet"] = "nanamammaa";
        $data["clientDeliveryAddressZipCode"] = "33-112";
        $data["clientDeliveryAddressCity"] = "tarnowiec";
        $data["clientDeliveryAddressCountry"] = "Polska";
        $data["clientDeliveryAddressPhone"] = "792544024";
        $data["payerAddressFirstName"] = "Rafał";
        $data["payerAddressLastName"] = "Ostrowski";
        $data["payerAddressFirm"] = "JKR";
        $data["payerAddressNip"] = "12548668";
        $data["payerAddressStreet"] = "tresdghj";
        $data["payerAddressZipCode"] = "33-112";
        $data["payerAddressCity"] = "Tarnowiec";
        $data["payerAddressCountryId"] = "PL";
        $data["payerAddressPhone"] = "792544024";
        //TODO tu usi być pętla z wszyskimi rzeczami z koszyka

        $data["productId"] = $id;
        $data["productQuantity"] = 1;
        $data["productBrutto"] = 39;
        $data["productIsFree"] = false;
        $data["productVat"] = 23;
        $data["clientRequestInvoice"] = "";

        $response = (new iaiHelper())->makeOrder($data);

        dump($response);
        die;
        }

// private function cURL($request,$address){
//
//     $request_json = json_encode($request);
//     $headers = [
//         'Accept: application/json',
//         'Content-Type: application/json;charset=UTF-8'
//     ];
//
//     $curl = curl_init($address);
//     curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//     curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
//     curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
//     curl_setopt($curl, CURLINFO_HEADER_OUT, 1);
//     curl_setopt($curl, CURLOPT_HEADER, 0);
//     curl_setopt($curl, CURLOPT_POST, 1);
//     curl_setopt($curl, CURLOPT_POSTFIELDS, $request_json);
//     curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//
//     $response = curl_exec($curl);
//     $status = curl_getinfo($curl);
//     curl_close($curl);
//
//     return ['response' => json_decode($response), 'status' => $status];
// }
}