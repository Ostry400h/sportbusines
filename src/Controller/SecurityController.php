<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 13.07.2018
 * Time: 23:36
 */

namespace App\Controller;

use App\Entity\Category;
use App\Form\LoginFormType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


/**
 * @ORM\Entity
 * @ORM\Table(name="security_controller")
 */
class SecurityController extends Controller
{

    /**
     *@Route("/login", name="security_login")
     */

    public function loginAction(AuthenticationUtils $authenticationUtils, EntityManagerInterface $em)
    {

        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginFormType::class, [
            '_username' => $lastUsername
        ]);

//        $form->handleRequest($request);

        return $this->render('security/login.html.twig', [
//            'last_username' => $lastUsername,
            'form'          => $form->createView(),
            'error'         => $error,
            'categories'      => $category,
        ]);
    }
    /**
     *@Route("/logout", name="security_logout")
     */

    public function logoutAction()
    {
        throw new \Exception("Logout");
    }


}