<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 01.08.2018
 * Time: 14:29
 */

namespace App\Controller;


use App\Entity\Category;
use App\Entity\User;
use App\Form\EditUserType;
use App\Form\UserRegistrationSimplyType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */

    public function RegisterSimplyUser(Request $request, AuthenticationUtils $authenticationUtils, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {

        $error = $authenticationUtils->getLastAuthenticationError();

        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        $user = new User();
        $form = $this->createForm(UserRegistrationSimplyType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
//
//            return $this->redirectToRoute('replace_with_some_route');
//        }
//
//            $user = $form->getData();
//            $enm = $this->getDoctrine()->getManager();
//            $enm->persist($user);
//            $enm->flush();

//            $this->get()
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->container->get('security.token_storage')->setToken($token);
            $this->container->get('session')->set('_security_main', serialize($token));

            $this->addFlash('success', 'Witaj ' . $user->getEmail());
            return $this->redirectToRoute('app_homepage');
        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
            'categories' => $category,
        ]);
//        $repository = $em->getRepository(Category::class);
//        $category = $repository->getCategories();

    }

    /**
     * @Route("/account", name="account")
     */

    public function editAccount(Request $request, EntityManagerInterface $em, UserInterface $user)
    {
        $repository = $em->getRepository(Category::class);
        $category = $repository->getCategories();

        $repository = $em->getRepository(User::class);
        $userData = $repository->find($user->getId());
        // get the login error if there is one
//        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
//        $lastUsername = $authenticationUtils->getLastUsername();


        $form = $this->createForm(EditUserType::class, $userData);
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userData = $form->getData();
            // 3) Encode the password (you could also do this via Doctrine listener)
//            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
//            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userData);
            $entityManager->flush();
//        $form->handleRequest($request);
        }
        return $this->render('user/account.html.twig', [
//            'last_username' => $lastUsername,
            'form' => $form->createView(),
            'categories' => $category
            ]);

}
}