<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $positionRedaction;

//    /**
//     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="personCompanies")
//     */
//    private $product;
//
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="company")
     */
    private $person;


    /**
     * @ORM\Column(type="date")
     */
    private $companyDate;




    public function __construct()
    {
        $this->person = new ArrayCollection();
//        $this->content = new ArrayCollection();

    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPositionRedaction(): ?string
    {
        return $this->positionRedaction;
    }

    public function setPositionRedaction(?string $positionRedaction): self
    {
        $this->positionRedaction = $positionRedaction;

        return $this;
    }

//    /**
//     * @return Collection|Product[]
//     */
//    public function getProduct(): Collection
//    {
//        return $this->product;
//    }
//
//    public function addProduct(Product $product): self
//    {
//        if (!$this->product->contains($product)) {
//            $this->product[] = $product;
//        }
//
//        return $this;
//    }
//
//    public function removeProduct(Product $product): self
//    {
//        if ($this->product->contains($product)) {
//            $this->product->removeElement($product);
//        }
//
//        return $this;
//    }
//
    /**
     * @return Collection|Person[]
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
        }

        return $this;
    }



    /**
     * @return mixed
     */
    public function getCompanyDate()
    {
        return $this->companyDate;
    }

    /**
     * @param mixed $companyDate
     */
    public function setCompanyDate($companyDate): void
    {
        $this->companyDate = $companyDate;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Category in the select
        $show = $this->name;
        return $show;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
