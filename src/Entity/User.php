<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity("email",
 *     message="Wygląda na to że masz już u nas konto")
 */
class User implements UserInterface
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(type="string", length=150 ,unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $password;
    /**
     * @Assert\NotBlank(groups={"Registration"})
     */

    private $plainPassword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="users")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->karts = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyPostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyCity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyTax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendPostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendCity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sendInfo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Kart", mappedBy="user")
     */
    private $karts;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $iaiId;


    public function getUsername()
    {
        return $this->email;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }

    public function getSalt()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;

//        return $this;
    }
    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        $this->password = null;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCompanyAddress(): ?string
    {
        return $this->companyAddress;
    }

    public function setCompanyAddress(?string $companyAddress): self
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    public function getCompanyPostal(): ?string
    {
        return $this->companyPostal;
    }

    public function setCompanyPostal(?string $companyPostal): self
    {
        $this->companyPostal = $companyPostal;

        return $this;
    }

    public function getCompanyCity(): ?string
    {
        return $this->companyCity;
    }

    public function setCompanyCity(?string $companyCity): self
    {
        $this->companyCity = $companyCity;

        return $this;
    }

    public function getCompanyTax(): ?string
    {
        return $this->companyTax;
    }

    public function setCompanyTax(?string $companyTax): self
    {
        $this->companyTax = $companyTax;

        return $this;
    }

    public function getSendName(): ?string
    {
        return $this->sendName;
    }

    public function setSendName(?string $sendName): self
    {
        $this->sendName = $sendName;

        return $this;
    }

    public function getSendAddress(): ?string
    {
        return $this->sendAddress;
    }

    public function setSendAddress(?string $sendAddress): self
    {
        $this->sendAddress = $sendAddress;

        return $this;
    }

    public function getSendPostal(): ?string
    {
        return $this->sendPostal;
    }

    public function setSendPostal(?string $sendPostal): self
    {
        $this->sendPostal = $sendPostal;

        return $this;
    }

    public function getSendCity(): ?string
    {
        return $this->sendCity;
    }

    public function setSendCity(?string $sendCity): self
    {
        $this->sendCity = $sendCity;

        return $this;
    }

    public function getSendInfo(): ?string
    {
        return $this->sendInfo;
    }

    public function setSendInfo(?string $sendInfo): self
    {
        $this->sendInfo = $sendInfo;

        return $this;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Category in the select
//        $show = $this->email;
//        return $show;
        // to show the id of the Category in the select
         return (string) $this->id;
    }

    /**
     * @return Collection|Kart[]
     */
    public function getKarts(): Collection
    {
        return $this->karts;
    }

    public function addKart(Kart $kart): self
    {
        if (!$this->karts->contains($kart)) {
            $this->karts[] = $kart;
            $kart->setUser($this);
        }

        return $this;
    }

    public function removeKart(Kart $kart): self
    {
        if ($this->karts->contains($kart)) {
            $this->karts->removeElement($kart);
            // set the owning side to null (unless already changed)
            if ($kart->getUser() === $this) {
                $kart->setUser(null);
            }
        }

        return $this;
    }

    public function getIaiId(): ?int
    {
        return $this->iaiId;
    }

    public function setIaiId(?int $iaiId): self
    {
        $this->iaiId = $iaiId;

        return $this;
    }

}
