<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KartRepository")
 */
class Kart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="karts")
     */
    private $user;


    /**
     * @ORM\Column(type="boolean")
     */
    private $isDone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentStatus;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $weight;

    /**
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @ORM\Column(type="integer")
     */
    private $productQuantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $productRetailPrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $productVat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productBundleItems;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $producerId;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version
     */
    private $createdAtValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tempUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productImg;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sizeId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $orderId;

//    public function __construct()
//    {
//        $this->productId = new ArrayCollection();
//    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(?string $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function setProductId(int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getProductQuantity(): ?int
    {
        return $this->productQuantity;
    }

    public function setProductQuantity(int $productQuantity): self
    {
        $this->productQuantity = $productQuantity;

        return $this;
    }

    public function getProductRetailPrice()
    {
        return $this->productRetailPrice;
    }

    public function setProductRetailPrice($productRetailPrice): self
    {
        $this->productRetailPrice = $productRetailPrice;

        return $this;
    }

    public function getProductVat()
    {
        return $this->productVat;
    }

    public function setProductVat($productVat): self
    {
        $this->productVat = $productVat;

        return $this;
    }

    public function getProductBundleItems(): ?string
    {
        return $this->productBundleItems;
    }

    public function setProductBundleItems(?string $productBundleItems): self
    {
        $this->productBundleItems = $productBundleItems;

        return $this;
    }

    public function getProducerId(): ?string
    {
        return $this->producerId;
    }

    public function setProducerId(?string $producerId): self
    {
        $this->producerId = $producerId;

        return $this;
    }

    public function getCreatedAtValue(): ?\DateTimeInterface
    {
        return $this->createdAtValue;
    }

    public function setCreatedAtValue(\DateTimeInterface $createdAtValue): self
    {
        $this->createdAtValue = $createdAtValue;

        return $this;
    }

    public function getTempUser(): ?string
    {
        return $this->tempUser;
    }

    public function setTempUser(string $tempUser): self
    {
        $this->tempUser = $tempUser;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(?string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getProductImg(): ?string
    {
        return $this->productImg;
    }

    public function setProductImg(?string $productImg): self
    {
        $this->productImg = $productImg;

        return $this;
    }

    public function getSizeId(): ?string
    {
        return $this->sizeId;
    }

    public function setSizeId(?string $sizeId): self
    {
        $this->sizeId = $sizeId;

        return $this;
    }

    public function getOrderId(): ?string
    {
        return $this->orderId;
    }

    public function setOrderId(?string $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }
}
