<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publisherId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companySendName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendPerson;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendPostcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendCity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendMail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sendPhone;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Content", inversedBy="person")
     */
    private $content;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Company", inversedBy="person")
     */
    private $company;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="person")
     */
    private $product;


    public function __construct()
    {
        $this->content = new ArrayCollection();
        $this->company = new ArrayCollection();
        $this->product = new ArrayCollection();

    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Category in the select
        $show = $this->name.' '.$this->surname;
        return $show;
        // to show the id of the Category in the select
        // return $this->id;
    }


    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPublisherId(): ?string
    {
        return $this->publisherId;
    }

    public function setPublisherId(?string $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getCompanySendName(): ?string
    {
        return $this->companySendName;
    }

    public function setCompanySendName(?string $companySendName): self
    {
        $this->companySendName = $companySendName;

        return $this;
    }

    public function getSendPerson(): ?string
    {
        return $this->sendPerson;
    }

    public function setSendPerson(?string $SendPerson): self
    {
        $this->sendPerson = $SendPerson;

        return $this;
    }

    public function getSendAddress(): ?string
    {
        return $this->sendAddress;
    }

    public function setSendAddress(?string $sendAddress): self
    {
        $this->sendAddress = $sendAddress;

        return $this;
    }

    public function getSendPostcode(): ?string
    {
        return $this->sendPostcode;
    }

    public function setSendPostcode(?string $sendPostcode): self
    {
        $this->sendPostcode = $sendPostcode;

        return $this;
    }

    public function getSendCity(): ?string
    {
        return $this->sendCity;
    }

    public function setSendCity(?string $sendCity): self
    {
        $this->sendCity = $sendCity;

        return $this;
    }

    public function getSendMail(): ?string
    {
        return $this->sendMail;
    }

    public function setSendMail(?string $sendMail): self
    {
        $this->sendMail = $sendMail;

        return $this;
    }

    public function getSendPhone(): ?string
    {
        return $this->sendPhone;
    }

    public function setSendPhone(?string $sendPhone): self
    {
        $this->sendPhone = $sendPhone;

        return $this;
    }
    /**
     * @return Collection|Content[]
     */
    public function getContent(): Collection
    {
        return $this->content;
    }

    public function addContent(Content $content): self
    {
        if (!$this->content->contains($content)) {
            $this->content[] = $content;
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->content->contains($content)) {
            $this->content->removeElement($content);
        }

        return $this;
    }
    /**
     * @return Collection|Company[]
     */
    public function getCompany(): Collection
    {
        return $this->company;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->company->contains($company)) {
            $this->company[] = $company;
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        if ($this->company->contains($company)) {
            $this->company->removeElement($company);
        }

        return $this;
    }
}
