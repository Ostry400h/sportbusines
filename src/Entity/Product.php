<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nr;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nrIssnIssb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publisher;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="date")
     */
    private $publishedDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Content", mappedBy="product")
     */
    private $contents;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductType", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publisherId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $shopId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="product")
     */
    private $person;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="products")
     */
    private $users;


    public function __construct()
    {
        $this->contents = new ArrayCollection();
        $this->person = new ArrayCollection();
        $this->parameter = new ArrayCollection();
//        $this->personCompanies = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNr(): ?int
    {
        return $this->nr;
    }

    public function setNr(int $nr): self
    {
        $this->nr = $nr;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNrIssnIssb(): ?string
    {
        return $this->nrIssnIssb;
    }

    public function setNrIssnIssb(?string $nrIssnIssb): self
    {
        $this->nrIssnIssb = $nrIssnIssb;

        return $this;
    }

    public function getPublisher(): ?string
    {
        return $this->publisher;
    }

    public function setPublisher(?string $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getPublishedDate(): ?\DateTimeInterface
    {
        return $this->publishedDate;
    }

    public function setPublishedDate(\DateTimeInterface $publishedDate): self
    {
        $this->publishedDate = $publishedDate;

        return $this;
    }

    /**
     * @return Collection|Content[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->addProduct($this);
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            $content->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
        }

        return $this;
    }

//    /**
//     * @return Collection|Parameter[]
//     */
//    public function getParameter(): Collection
//    {
//        return $this->parameter;
//    }
//
//    public function addParameter(Parameter $parameter): self
//    {
//        if (!$this->parameter->contains($parameter)) {
//            $this->parameter[] = $parameter;
//        }
//
//        return $this;
//    }
//
//    public function removeParameter(Parameter $parameter): self
//    {
//        if ($this->parameter->contains($parameter)) {
//            $this->parameter->removeElement($parameter);
//        }
//
//        return $this;
//    }



    public function getType(): ?ProductType
    {
        return $this->type;
    }

    public function setType(?ProductType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPublisherId(): ?string
    {
        return $this->publisherId;
    }

    public function setPublisherId(?string $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getShopId(): ?int
    {
        return $this->shopId;
    }

    public function setShopId(?int $shopId): self
    {
        $this->shopId = $shopId;

        return $this;
    }
    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Category in the select
        $show = $this->publisherId.'-'.$this->name.' '.$this->title;
        return $show;
        // to show the id of the Category in the select
        // return $this->id;
    }

//    /**
//     * @return Collection|Company[]
//     */
//    public function getPersonCompanies(): Collection
//    {
//        return $this->personCompanies;
//    }
//
//    public function addPersonCompany(Company $personCompany): self
//    {
//        if (!$this->personCompanies->contains($personCompany)) {
//            $this->personCompanies[] = $personCompany;
//            $personCompany->addProduct($this);
//        }
//
//        return $this;
//    }
//
//    public function removePersonCompany(Company $personCompany): self
//    {
//        if ($this->personCompanies->contains($personCompany)) {
//            $this->personCompanies->removeElement($personCompany);
//            $personCompany->removeProduct($this);
//        }
//
//        return $this;
//    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addProduct($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeProduct($this);
        }

        return $this;
    }

}
