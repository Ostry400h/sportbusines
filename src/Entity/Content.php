<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContentRepository")
 */
class Content
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $availability;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(type="date")
     */
    private $publishedDate;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="contents")
     */
    private $category;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="contents")
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Content")

     */
    private $contentRelation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contentValue;

/**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderRange;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ContentType", inversedBy="contents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publisherId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="content")
     */
    private $person;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $linkPage;


    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->person = new ArrayCollection();
        $this->product = new ArrayCollection();
        $this->contentRelation = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAvailability(): ?string
    {
        return $this->availability;
    }

    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getPublishedDate(): ?\DateTimeInterface
    {
        return $this->publishedDate;
    }

    public function setPublishedDate(\DateTimeInterface $publishedDate): self
    {
        $this->publishedDate = $publishedDate;

        return $this;
    }


    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }

    /**
     * @return Collection|Content[]
     */
    public function getContentRelation(): Collection
    {
        return $this->contentRelation;
    }

    public function addContentRelation(Content $contentRelation): self
    {
        if (!$this->contentRelation->contains($contentRelation)) {
            $this->contentRelation[] = $contentRelation;
        }

        return $this;
    }

    public function removeContentRelation(Content $contentRelation): self
    {
        if ($this->contentRelation->contains($contentRelation)) {
            $this->contentRelation->removeElement($contentRelation);
        }

        return $this;
    }


    public function getContentValue(): ?string
    {
        return $this->contentValue;
    }

    public function setContentValue(?string $contentValue): self
    {
        $this->contentValue = $contentValue;

        return $this;
    }

    public function getType(): ?ContentType
    {
        return $this->type;
    }

    public function setType(?ContentType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPublisherId(): ?string
    {
        return $this->publisherId;
    }

    public function setPublisherId(?string $publisherId): self
    {
        $this->publisherId = $publisherId;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

//    /**
//     * @return mixed
//     */
//    public function getLink(): ?string
//    {
//        return $this->link;
//    }
//
//    /**
//     * @param mixed $title
//     */
//    public function setLink()
//    {
////        $this->link = str_replace(' ','+',$title);
//        $this->link = 'cos';
//        return $this;
//    }

    public function getLinkPage(): ?string
    {
        return $this->linkPage;
    }

    public function setLinkPage(?string $linkPage): self
    {
        $this->linkPage = str_replace(' ','-',$linkPage);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderRange()
    {
        return $this->orderRange;
    }

    /**
     * @param mixed $orderRange
     */
    public function setOrderRange($orderRange): void
    {
        $this->orderRange = $orderRange;
    }
    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Category in the select
        $show = $this->publisherId.'-'.$this->title;
        return $show;
        // to show the id of the Category in the select
        // return $this->id;
    }

}
