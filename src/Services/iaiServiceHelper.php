<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 28.08.2018
 * Time: 12:41
 */

namespace App\Services;


class iaiServiceHelper
{


    public function shopProducts()
    {

        $address = 'http://sb360.iai-shop.com/api/?gate=products/get/77/json';

        $request = [];
        $request['params'] = [];
        $request['params']['returnProducts'] = "active";
        $request['params']['productShops'] = [];
        $request['params']['productShops'][0]['shopId'] = 3;
        $request['params']['productType'] = array();
        $request['params']['productType']['productTypeInItem'] = true;
        $request['params']['productType']['productTypeInFree'] = true;
        $request['params']['productType']['productTypeInBundle'] = true;
        $request['params']['productType']['productTypeInCollection'] = true;
        $request['params']['productType']['productTypeInPackaging'] = true;
        $request['params']['productType']['productTypeInService'] = true;

        $response = $this->cURL($request, $address);

        return $response;
    }

    public function addUser($fillData)
    {
        $address = 'http://sb360.iai-shop.com/api/?gate=clients/addClients/77/json';

        $request = [];
        $request['params'] = [];
        $request['params']['clients'] = [];
        $request['params']['clients'][0] = [];
        $request['params']['clients'][0]['login'] = $fillData->getEmail();
        $request['params']['clients'][0]['code_extern'] = $fillData->getId();
        $request['params']['clients'][0]['email'] = $fillData->getEmail();
        $request['params']['clients'][0]['firstname'] = $fillData->getName();
        $request['params']['clients'][0]['lastname'] = $fillData->getSurname();
        $request['params']['clients'][0]['street'] = $fillData->getSendAddress();
        $request['params']['clients'][0]['zipcode'] = $fillData->getSendPostal();
        $request['params']['clients'][0]['city'] = $fillData->getSendCity();
//$request['params']['clients'][0]['country_code'] = $fillData["country_code"];
//$request['params']['clients'][0]['province_code'] = $fillData["province_code"];
        $request['params']['clients'][0]['password'] = uniqid();
//$request['params']['clients'][0]['birth_date'] = $fillData["birth_date"];
        $request['params']['clients'][0]['phone'] = $fillData->getPhone();
        $request['params']['clients'][0]['company'] = $fillData->getCompanyName();
//        $request['params']['clients'][0]['vat_number'] = $fillData["vat_number"];
//$request['params']['clients'][0]['wholesaler'] = true;
//$request['params']['clients'][0]['client_type'] = 'person';
//$request['params']['clients'][0]['language'] = "language";
        $request['params']['clients'][0]['shops'] = [];
        $request['params']['clients'][0]['shops'][0] = 3;
//$request['params']['clients'][0]['block_autosigning_to_shops'] = true;
//$request['params']['clients'][0]['currency'] = "currency";
//$request['params']['clients'][0]['delivery_dates'] = [];
//$request['params']['clients'][0]['delivery_dates'][0] = "delivery_dates";
//$request['params']['clients'][0]['external_balance_value'] = 2.0;
//$request['params']['clients'][0]['external_trade_credit_limit_value'] = 3.0;
//$request['params']['clients'][0]['email_newsletter'] = true;
//$request['params']['clients'][0]['request_reference'] = "request_reference";
//$request['params']['clients'][0]['newsletter_email_approvals'] = [];
//$request['params']['clients'][0]['newsletter_email_approvals'][0] = [];
//$request['params']['clients'][0]['newsletter_email_approvals'][0]['approval'] = 'y';
//$request['params']['clients'][0]['newsletter_email_approvals'][0]['shop_id'] = 3;
        $request['settings'] = [];
        $request['settings']['send_mail'] = true;

        $response = $this->cURL($request, $address);

        return $response;
    }

    public function makeOrder($fillData)
    {
        $address = 'http://sb360.iai-shop.com/api/?gate=orders/insert/52/json';

        $request = [];
        $request['params'] = [];
        $request['params']['orders'] = [];
        $request['params']['orders'][0] = [];
//    $request['params']['orders'][0]['orderType'] = $fillData["orderType"];
        $request['params']['orders'][0]['shopId'] = 3;
        $request['params']['orders'][0]['stockId'] = 5;
        $request['params']['orders'][0]['orderPaymentType'] = 'cash_on_delivery';
        $request['params']['orders'][0]['currencyId'] = 'PLN';
        $request['params']['orders'][0]['clientLogin'] = $fillData["form"]->getEmail();
        $request['params']['orders'][0]['clientNoteToOrder'] = $fillData["form"]->getSendInfo();
//$request['params']['orders'][0]['affiliateId'] = 3;
        $request['params']['orders'][0]['clientWithoutAccount'] = "n";
        $request['params']['orders'][0]['courierId'] = 1;
        $request['params']['orders'][0]['deliveryCost'] = $fillData["price"]['shipping'];
        $request['params']['orders'][0]['clientDeliveryAddress'] = [];
        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressFirstName'] = $fillData["form"]->getName();
        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressLastName'] = $fillData["form"]->getSurname();
//        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressAdditional'] = $fillData["form"]["send"];
        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressStreet'] = $fillData["form"]->getSendAddress();
        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressZipCode'] = $fillData["form"]->getSendPostal();
        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressCity'] = $fillData["form"]->getSendCity();
        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressCountry'] = "Polska";
//        $request['params']['orders'][0]['clientDeliveryAddress']['clientDeliveryAddressPhone'] = $fillData["form"]["send"];
        $request['params']['orders'][0]['payerAddress'] = [];
//$request['params']['orders'][0]['payerAddress']['payerAddressId'] = 6;
        $request['params']['orders'][0]['payerAddress']['payerAddressFirstName'] = $fillData["form"]->getName();
        $request['params']['orders'][0]['payerAddress']['payerAddressLastName'] = $fillData["form"]->getSurname();
        $request['params']['orders'][0]['payerAddress']['payerAddressFirm'] = $fillData["form"]->getCompanyName();
        $request['params']['orders'][0]['payerAddress']['payerAddressNip'] = $fillData["form"]->getCompanyTax();
        $request['params']['orders'][0]['payerAddress']['payerAddressStreet'] = $fillData["form"]->getCompanyAddress();
        $request['params']['orders'][0]['payerAddress']['payerAddressZipCode'] = $fillData["form"]->getCompanyPostal();
        $request['params']['orders'][0]['payerAddress']['payerAddressCity'] = $fillData["form"]->getCompanyCity();
        $request['params']['orders'][0]['payerAddress']['payerAddressCountryId'] = 'PL';
//        $request['params']['orders'][0]['payerAddress']['payerAddressPhone'] = $fillData["form"]["company"];
        $request['params']['orders'][0]['products'] = [];

        $i = 0;
        foreach ($fillData["kart"] as $data) {
//            dump($data);die;
            $request['params']['orders'][0]['products'][$i] = [];
//        dump($fillData["kart"][0][0]->getProductId());die;
            $request['params']['orders'][0]['products'][$i]['productId'] = $data[0]->getProductId();
            if($data[0]->getSizeId() != '-1') $request['params']['orders'][0]['products'][$i]['sizeId'] = $data[0]->getSizeId();
//$request['params']['orders'][0]['products'][$i]['productSizeCodeExternal'] = $fillData["productSizeCodeExternal"];
//$request['params']['orders'][0]['products'][$i]['stockId'] = 8;
            $request['params']['orders'][0]['products'][$i]['productQuantity'] = $data[0]->getProductQuantity();
            $request['params']['orders'][0]['products'][$i]['productRetailPrice'] = $data[0]->getProductRetailPrice();
            $request['params']['orders'][0]['products'][$i]['productFree'] = false;
            $request['params']['orders'][0]['products'][$i]['productVat'] = $data[0]->getProductVat();
//$request['params']['orders'][0]['products'][0]['discountCode'] = [];
//$request['params']['orders'][0]['products'][0]['discountCode']['name'] = $fillData["name"];
//$request['params']['orders'][0]['products'][0]['remarksToProduct'] = $fillData["remarksToProduct"];
            $a = 0;
            if($data[0]->getProductBundleItems() != null) {

                $request['params']['orders'][0]['products'][$i]['productBundleItems'] = [];
                foreach (explode(',',$data[0]->getProductBundleItems()) as $dataBundle) {
                    $request['params']['orders'][0]['products'][$i]['productBundleItems'][$a] = [];
                    $request['params']['orders'][0]['products'][$i]['productBundleItems'][$a]['productId'] = $dataBundle;
                    $request['params']['orders'][0]['products'][$i]['productBundleItems'][$a]['sizeId'] = "uniw";
                    $request['params']['orders'][0]['products'][$i]['productBundleItems'][$a]['sizePanelName'] = "uniw";
//                    $request['params']['orders'][0]['products'][$i]['productBundleItems'][0]['productIndex'] = $fillData["productIndex"];
                    $a++;
                }
            }
            $i++;
        }
//$request['params']['orders'][0]['wrappers'] = [];
//$request['params']['orders'][0]['wrappers'][0] = [];
//$request['params']['orders'][0]['wrappers'][0]['wrapperId'] = 13;
//$request['params']['orders'][0]['wrappers'][0]['stockId'] = 14;
//$request['params']['orders'][0]['wrappers'][0]['wrapperQuantity'] = 15.0;
//$request['params']['orders'][0]['wrappers'][0]['wrapperRetailPrice'] = 16.0;
//$request['params']['orders'][0]['orderRebateValue'] = 17.0;
//$request['params']['orders'][0]['orderOperatorLogin'] = $fillData["orderOperatorLogin"];
        $request['params']['orders'][0]['ignoreBridge'] = true;
        $request['params']['orders'][0]['settings'] = [];
        $request['params']['orders'][0]['settings']['settingSendMail'] = true;
//$request['params']['orders'][0]['settings']['settingSendSMS'] = true;
        $request['params']['orders'][0]['clientRequestInvoice'] = "";

//        dump($request);die;
        $response = $this->cURL($request, $address);

        return $response;
    }

    private function cURL($requestPart, $address)
    {

        $authentication = sha1(date('Ymd') . sha1('nicnicnic'));

        $requestMain = [];
        $requestMain['authenticate'] = [];
        $requestMain['authenticate']['userLogin'] = "APISB360"; //TODO: brac ze stałej albo z env /
        $requestMain['authenticate']['system_login'] = "APISB360"; //TODO: brac ze stałej albo z env /
        $requestMain['authenticate']['authenticateKey'] = $authentication; //TODO: brac ze stałej albo z env
        $requestMain['authenticate']['system_key'] = $authentication; //TODO: brac ze stałej albo z env

        $request = array_merge($requestMain, $requestPart);

        $request_json = json_encode($request);
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json;charset=UTF-8'
        ];

        $curl = curl_init($address);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($curl, CURLINFO_HEADER_OUT, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request_json);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($curl);
        $status = curl_getinfo($curl);
        curl_close($curl);

        return ['response' => json_decode($response), 'status' => $status];
    }
}