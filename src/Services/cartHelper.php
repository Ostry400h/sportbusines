<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 06.09.2018
 * Time: 11:22
 */

namespace App\Services;


use App\Entity\Kart;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class cartHelper
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function addToCartHelper($id, $user)
    {

        $response = (new iaiServiceHelper())->shopProducts();
        $userId = ($user != null) ? $user->getId() : $userId = 1000;

        foreach ($response['response']->results as $product ) {
//            dump($product->productId);
            if($product->productId == $id){

//                $entityManager = $em->getDoctrine()->getManager();

                $kart = new Kart();
                $kart->setProductId($id);
                $kart->setProductQuantity(1);
                $kart->setSizeId($product->sizesGroupId);
                $kart->setProductName($product->productDescriptionsLangData[0]->productName);
                $kart->setProductImg($product->productIcon->productIconSmallUrl);
                $kart->setProducerId($product->producerId);
                $kart->setProductRetailPrice($product->productRetailPrice);
                $kart->setProductVat($product->productVat);
                $kart->setWeight($product->productWeight);
                if(!empty($product->productBundleItems)){
                    $bundle = [];

                    foreach($product->productBundleItems as $bundleItem) {
                        $bundle[] = $bundleItem->productId;
                    }
                    $bundles = implode(",",$bundle);
                    $kart->setProductBundleItems($bundles);
                }
                $kart->setIsDone(false);
                if($user != null) {
                    $kart->setUser($user);
                }else{
                    $kart->setTempUser($userId);
                }

                // tell Doctrine you want to (eventually) save the Product (no queries yet)
                $this->em->persist($kart);

                // actually executes the queries (i.e. the INSERT query)
                $this->em->flush();
            }

//            $repositoryKart = $em->getRepository(Kart::class);
//        $kartData = $repositoryKart->getUserCart($userId,$userType);
//
//        $kartTotal = $repositoryKart->getUserTotal($userId,$userType);
//
//        $priceTotal = (new iaiHelper())->countShippingPrice($kartTotal);

//            return $this->redirectToRoute('cart');

//       return $this->render('koszyk.html.twig',
//        [
//            'categories' => $category,
//            'data' => $kartData,
//            'userType' => $userType,
//            'priceTotal' => $priceTotal
//
//        ]);
        }
    }
    public function countShippingPrice($array)
    {
        if($array[0]['sumweight'] < 400 )
            $kartShipping = 8.00;
        elseif ($array[0]['sumweight'] < 901)
            $kartShipping = 15.00;
        else
            $kartShipping = 25.00;

        $total = $kartShipping + $array[0]['total'];

        $kartShippingTotal = ['shipping' => $kartShipping,'finalTotal' => $total];
        $array = array_merge($array[0],$kartShippingTotal);

        return $array;
    }
}