<?php
/**
 * Created by PhpStorm.
 * User: ostry
 * Date: 04.01.2019
 * Time: 00:48
 */

namespace App\EventListener\Przelewy24;

use Allset\Przelewy24Bundle\Event\PaymentEventInterfce;
use App\Entity\Kart;
use App\Entity\Payment as PaymentDB;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class PaymentSuccessListener
{

    public function onPrzelewy24EventPaymentSuccess(PaymentEventInterfce $event, EntityManagerInterface $em)
    {
        $token = $event->getPayment()->getSessionId();

        $repository = $em->getRepository(PaymentDB::class);
        $update = $repository->findOneBy(['uniq'=>'5c2eb4c6e75e7']);
        $update->setPaid(new \DateTime('now'));

        $payment = new PaymentDB();
        $payment->setOrderId('nic');
        $payment->setPrice(10 * 100);
        $payment->setUniq(uniqid());

        //        $repository = $em->getRepository(Kart::class);
//        $find = $repository->findBy(['orderId' => $token]);
        $em->persist($payment);
        $em->persist($update);
        $em->flush();
//        dump($find);
//        foreach ($find as $obj) {
////            $obj[0]->setOrderId($orderId);
////            $em->flush();
//
//            //TU JEST TO CO TRZEBA ZUPDATWA PO Płatności ID wydania do usera
//
//                    $repository = $em->getRepository(User::class);
//                    $userProd = $repository->findOneBy(['id' => $obj->getId()]);
//
//                    $product = $em->getRepository(Product::class);
//                    $repo = $product->findOneBy(['shopId' => $obj->getProductId()]);
//                    $userProd->addProduct($repo);
//
//                    $em->flush();
//        }
//        $em->flush();
        // ..

    }
}