<!DOCTYPE html>
<html lang="pl">

 <head>
	 <meta name="viewport" content="width=device-width, initial-scale=1" />
	 <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	 <meta name="author" content="INSPIRO" />
	 <meta name="description" content="Sport&Business 360 ">
	 <!-- Document title -->
	 <title>Sport&Business 360</title>
	 <!-- Stylesheets & Fonts -->
	 <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
	 <link href="css/plugins.css" rel="stylesheet"> <link href="css/style.css" rel="stylesheet"> <link href="css/responsive.css" rel="stylesheet">
 </head>

<body>	<!-- Wrapper -->
	<div id="wrapper" >

		 <!-- Topbar -->
        <div id="topbar" class="visible-md visible-lg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="top-menu">
                            <li><a href="#">MAGAZYN</a></li>
                            <li><a href="#">REKLAMA</a></li>
                            <li><a href="#">PARTNERSTWO</a></li>
                            <li><a href="#">KONTAKT</a></li>
                        </ul>
                    </div>
					
                    <div class="col-sm-3 hidden-xs">
                        <div class="social-icons social-icons-colored-hover">
                            <ul>
                                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
								
                            </ul>
							
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
        <!-- end: Topbar -->

        <!-- Header -->
        <header id="header" class="header-dark dark">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="index.html" class="logo" data-dark-logo="images/logo-dark.png">
                            <img src="images/logo-white.png" alt="Polo Logo">
                        </a>
                    </div>
                    <!--End: Logo-->
					
					
					<!---- SPRAWDZIĆ CO TO ---------------------- ------------------------ ---------------------- ------------------------>
					
					
                    <div class="header-extras">
                        <ul>
                            <li class="hidden-xs">
                                <!--shopping cart-->
                                <div id="shopping-cart">
                                    <a href="shop-cart.html">
                                        <span class="shopping-cart-items">8</span>

                                        <i class="fa fa-shopping-cart"></i></a>
                                </div>
								
                                <!--end: shopping cart-->
                            </li>
							<li>
                                <div class="topbar-dropdown">
                                    <a class="title"><i class="fa fa-lock"></i></a>
                                    <div class="dropdown-list">
                                        <a class="list-entry" href="#">Zaloguj się</a>
                                        <a class="list-entry" href="#">Załóż konto</a>
                                        <a class="list-entry" href="#">Wyloguj się</a>
                                    </div>
                                </div>
                            </li>
                           
                        </ul>
                    </div>
                    
                   

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->
					
					<!---- SPRAWDZIĆ CO TO ------------------------------------ ------------------------ -------- KONIEC------------------------ >
					
                    <!--Navigation-->
                    <div id="mainMenu" >
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="#">Wydania</a></li>
									<li><a href="#">Sklep</a></li>
									<li><a href="#">Wydarzenia</a></li>
                                    
                                    <li class="dropdown mega-menu-item"><a href="#">Twój SB360 <i class="fa fa-angle-down" aria-hidden="true"> </i></a>
										<ul class="dropdown-menu">
                                            <li class="mega-menu-content">
                                                <div class="row">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <ul>
                                                            <li> <a href="shop-columns-2.html"><i class="fa fa-rocket"></i>Rozwój kariery</a> </li>
                                                            <li> <a href="shop-columns-2.html"><i class="fa fa-map-signs"></i>Dwutorowość kariery</a> </li>
                                                            <li> <a href="shop-columns-2.html"><i class="fa fa-money"></i>Formy finansowania</a> </li>
                                                            <li> <a href="shop-columns-3.html"><i class="fa fa-line-chart"></i>Zarządzanie organizacją</a> </li>
                                                            <li> <a href="shop-columns-4.html"><i class="fa fa-heart"></i>CSR w sporcie</a> </li>
                                                            <li> <a href="shop-columns-6.html"><i class="fa fa-bank"></i>Sport w samorządach</a> </li>
                                                            <li> <a href="shop-sidebar-sticky.html"><i class="fa fa-code"></i>Technologia w sporcie</a> </li>
                                                            <li> <a href="shop-wide.html"><i class="fa fa-balance-scale"></i>Prawo & księgowość</a> </li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <ul>
                                                            
                                                            <li> <a href="shop-columns-4.html"><i class="fa fa-futbol-o"></i>Marketing sportowy</a> </li>
                                                            <li> <a href="shop-columns-5.html"><i class="fa fa-handshake-o"></i>Sponsoring</a> </li>
															<li> <a href="shop-columns-2.html"><i class="fa fa-users"></i>Klub Biznesu</a> </li>
                                                            <li> <a href="shop-columns-3.html"><i class="fa fa-ticket"></i>Infrastruktura sportowa</a> </li>
                                                            
                                                            <li> <a href="shop-columns-5.html"><i class="fa fa-shopping-cart"></i>Sklep kibica</a> </li>
                                                            <li> <a href="shop-columns-6.html"><i class="fa fa-graduation-cap"></i>Kuźnia managerów</a> </li>
                                                            <li> <a href="shop-sidebar-sticky.html"><i class="fa fa-flag-checkered"></i>Organizacja imprez</a><br><br></li>
															
															<li>
																<div class="social-icons social-icons-colored-hover">
																	<ul>
																		<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
																		<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
																		<li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
																		
																	</ul>
																</div>
															</li>
														</ul>
                                                    </div>
													<div class="col-md-2 ">
														<ul>
															<li> <a href="shop-cart.html">Załóż konto</a> </li>
                                                            <li> <a href="shop-cart.html">Zaloguj się</a><br> </li>
															<li> <a href="shop-cart.html">Biblioteczka</a><br> </li>
															<li> <a href="shop-cart.html">Sklep</a> </li>
															<li> <a href="shop-cart.html">Prenumerata</a> <br></li>
														</ul>
                          							</div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
								 
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->

                </div>
            </div>
        </header>
        <!-- end: Header -->

	
	 
	<!-- SLIDER  ŚWIĘTA
	<section id="slider" class="fullscreen background-image" style="background-image:url(homepages/holiday/images/1.jpg);">
		<div class="container">
			<div class="container-fullscreen text-center">
				<div class="text-middle">
					<h3 data-animation="fadeInDown" data-animation-delay="600" class="text-dark">Redakcja SB360 życzy wszystkim czytelnikom</h3>
					<h1 data-animation="fadeInDown" data-animation-delay="500" class="text-large" style="color:#e52a2d !important;">Wesołych Świąt</h1>
					<div data-animation="fadeInDown" data-animation-delay="600"> <a class="btn btn-dark" href="#"><span>Odbierz prezent</span></a> </div>
					<div class="scrolldown-animation" id="scroll-down"> <a class="scroll-to" href="#easy-fast"><img src="images/scrolldown.png" alt=""> </a> </div>
				</div>
			</div>
		</div>
	</section>
	end: SLIDER --> 
	
	
	

	
	
		 <!-- Page Menu -->
        <div class="page-menu menu-lines">
            <div class="container">
                <div class="menu-title">Twój SB360</div>
                <nav>
                    <ul>
						<li><a href="page-menu.html">Biblioteczka</a> </li>
						<li><a href="page-menu.html">Ulubione</a> </li>
                        <li><a href="page-menu-outline.html">Zamówienia</a> </li>
                        <li class="active"><a href="page-menu-lines.html">Ustawienia profilu</a> </li>
                    </ul>
                </nav>

                <div id="menu-responsive-icon">
                    <i class="fa fa-reorder"></i>
                </div>

            </div>
        </div>
        <!-- end: Page Menu -->
	
	
